<?php
function insertLocation($data)
{
    global $pdo;
    $sql="INSERT INTO `locations` (`title`,`lat`,`lng`,`type`) VALUES (:title,:lat,:lng,:typ)";
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['title'=>$data['title'],'lat'=>$data['lat'],'lng'=>$data['lng'],'typ'=>$data['type']]);
    return $stmt->rowCount();
}

function getLocations($params=[]){
    global $pdo;
    $conditions='';
    if(isset($params['keyword'])){
        $conditions="WHERE verified = 1 and title like '%{$params['keyword']}%'";
    }
    $sql="SELECT * FROM  `locations` $conditions";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_OBJ);
}