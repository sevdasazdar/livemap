<?php
$database_config = (object)[
    'host' => 'localhost',
    'user' => 'root',
    'pass' => 'root',
    'db' => '7map',
    'port'=>8889

];

$admins = [
    'admin' => '$2y$10$sAubqKZ2YHSNaB1C3uTAQe14oGaJ.hsrzRjlc0aMQtYmWu5SfAD6m',
    'sevda' => '$2y$10$F7aDuHd0vYbOnGYcO8fDA.epAV1zaU8DNPa2bxuDYf6srbiUoF6UG'
];
// echo password_hash('sevda',PASSWORD_BCRYPT);