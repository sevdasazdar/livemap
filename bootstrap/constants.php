<?php
define('SITE_TITLE','liveMap Project');
define('BASE_URL','http://localhost:8888/7learn/projects/livemap/');
define('BASE_PATH','/Applications/MAMP/htdocs/7learn/projects/livemap');


const locationTypes = [
    0 => "عمومی",
    1 => "شرکت",
    2 => "رستوران",
    3 => "پارک",
    4 => "فروشگاه",
    5 => "پاساژ",
    6 => "قهوه خونه",
    7 => 'دانشگاه'
];