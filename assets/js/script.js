const defaltLocation = [35.7414648, 51.3223126];
const defaltZoom = 15;
document.getElementById('map').style.setProperty('height', window.innerHeight + "px");

const map = L.map('map').setView(defaltLocation, defaltZoom);

const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,

}).addTo(map);

// L.marker(defaltLocation).addTo(map).bindPopup("sevda home1").openPopup();
// L.marker([35.712, 51.338]).addTo(map).bindPopup("sevda work").openPopup();

// var northLine = map.getBounds().getNorth();
// var westLine = map.getBounds().getWest();
// var southLine = map.getBounds().getSouth();
// var eastLine = map.getBounds().getEast();

map.on('dblclick', function (event) {
    L.marker(event.latlng).addTo(map);
    $('.modal-overlay').fadeIn(500);
    $('#lat-display').val(event.latlng.lat);
    $('#lng-display').val(event.latlng.lng);
    $('#l-type').val(0);
    $('#l-title').val('');
});


$('#addLocationForm').submit(function (e) {
    e.preventDefault(); // prevent form submiting
    var form = $(this);
    var resultTag = form.find('.ajax-result');
    $.ajax({
        url: form.attr('data_action'),
        method: form.attr('method'),
        data: form.serialize(),
        success: function (response) {
            console.log(response);
            resultTag.html(response);
        }
    });

});


$('.close').on('click', function () {
    $('.modal-overlay').hide();
})